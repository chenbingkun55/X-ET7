/** This is an automatically generated class by FUICodeSpawner. Please do not modify it. **/

using FairyGUI;

namespace ET.Client
{
	public class HotUpdateBinder
	{
		public static void BindAll()
		{
			UIObjectFactory.SetPackageItemExtension(ET.Client.HotUpdate.FUI_HotUpdatePanel.URL, typeof(ET.Client.HotUpdate.FUI_HotUpdatePanel));
		}
	}
}
